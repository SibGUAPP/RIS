---
layout: post
title:  "Подключение матричной клавиатуры к Arduino"
date:   2017-07-23 15:00:00 -0300
categories: Arduino
---
Иногда мы сталкиваемся с проблемой нехватки портов на Arduino. Чаще всего это относится к моделям с небольшим количеством выводов. Для этого была придумана матричная клавиатура. Такая система работает в компьютерных клавиатурах, калькуляторах, телефонах и других устройств, в которых используется большое количество кнопок.

Для Arduino чаще всего используются такие клавиатуры:

![4x4 Matrix 16 Keypad Keyboard Module 16 Button][1]      ![Matrix Keyboard 16 Key][2]      ![Matrix Keyboard 16 Key Membrane Switch Keypad for arduino][3]

Самыми распространенными являются 16 кнопочные клавиатуры 4x4. Принцип их работы достаточно прост, Arduino поочередно подает логическую единицу на каждый 4 столбцов, в этот момент 4 входа Arduino считывают значения, и только на один вход подается высокий уровень. Это довольно просто, если знать возможности [управления портами вывода в Arduino][4], а так же портами входа/ввода.

 

![][5]

Для программирования можно использовать специализированную библиотеку Keypad, но в этой статье мы не будем её использовать для большего понимания работы с матричной клавиатуры.

Подключаем клавиатуру в любые порты ввода/вывода.

![][6]

На красные порты будем подавать сигналы, а с синих будем их принимать. Зачастую на синие провода подводят подтягивающие резисторы, но мы их подключим внутри микроконтроллера [Arduino][7].

В программе будем вычислять нажатую кнопку и записывать её в Serial порт.  
В данном методе есть один значительный недостаток: контроллер уже не может выполнять других задач стандартными методами. Эта проблем решается [подключением матричной клавиатуры с использованием прерываний][8].

 

 
    
    
    int PinOut[4] {5, 4, 3, 2}; // пины выходы
    
    int PinIn[4] {9, 8, 7, 6}; // пины входа
    int val = 0;
    const char value[4][4]
    
    
    { {'1', '4', '7', '*'},
      {'2', '5', '8', '0' },
      {'3', '6', '9', '#'},
      {'A', 'B', 'C', 'D'}
    };
    // двойной массив, обозначающий кнопку
    
    int b = 0; // переменная, куда кладется число из массива(номер кнопки)
    
    void setup()
    {
      pinMode (2, OUTPUT); // инициализируем порты на выход (подают нули на столбцы)
      pinMode (3, OUTPUT);
      pinMode (4, OUTPUT);
      pinMode (5, OUTPUT);
    
      pinMode (6, INPUT); // инициализируем порты на вход с подтяжкой к плюсу (принимают нули на строках)
      digitalWrite(6, HIGH);
      pinMode (7, INPUT);
      digitalWrite(7, HIGH);
      pinMode (8, INPUT);
      digitalWrite(8, HIGH);
      pinMode (9, INPUT);
      digitalWrite(9, HIGH);
    
      Serial.begin(9600); // открываем Serial порт
    }
    
    void matrix () // создаем функцию для чтения кнопок
    {
      for (int i = 1; i <= 4; i++) // цикл, передающий 0 по всем столбцам
      {
        digitalWrite(PinOut[i - 1], LOW); // если i меньше 4 , то отправляем 0 на ножку
        for (int j = 1; j <= 4; j++) // цикл, принимающих 0 по строкам
        {
          if (digitalRead(PinIn[j - 1]) == LOW) // если один из указанных портов входа равен 0, то..
          {
            Serial.println( value[i - 1][j - 1]); // то b равно значению из двойного массива
            delay(175);
          }
        }
        digitalWrite(PinOut[i - 1], HIGH); // подаём обратно высокий уровень
      }
    }
    
    void loop()
    {
      matrix(); // используем функцию опроса матричной клавиатуры
    
    }
    

 

С использованием библиотеки считывание данных с цифровой клавиатуры упрощается.

 
    
```    
#include <Keypad.h>

const byte ROWS = 4; 
const byte COLS = 3; 
char keys[ROWS][COLS] = {
  {'1','2','3'},
  {'4','5','6'},
  {'7','8','9'},
  {'#','0','*'}
};
byte rowPins[ROWS] = {5, 4, 3, 2}; 
byte colPins[COLS] = {8, 7, 6}; 
 
Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );
 
void setup(){
  Serial.begin(9600);
}
 
void loop(){
  char key = keypad.getKey();
 
  if (key != NO_KEY){
    Serial.println(key);
  }
}
```    
[Source](http://robots4life.ru/arduino-keypad "Permalink to Подключение матричной клавиатуры к Arduino")

[1]: http://robots4life.ru/sites/default/files/articles/th-4x4_matrix_16_keypad_keyboard_module_16_button-240x180.jpg
[2]: http://robots4life.ru/sites/default/files/articles/th-matrix_keyboard_16_key-240x180.png
[3]: http://robots4life.ru/sites/default/files/articles/th-matrix_keyboard_4x4_16_key-240x180.jpg
[4]: http://robots4life.ru/upravlenie-portami-vyvoda-arduino
[5]: http://robots4life.ru/sites/default/files/articles/image005.png
[6]: http://robots4life.ru/sites/default/files/articles/shema_podklyuchenie_arduino_i_matrix_key.png
[7]: http://robots4life.ru/arduino
[8]: http://robots4life.ru/matrichnaya-klaviatura-preryvania