---
layout: post
title:  "Датчик измерения расстояния SHARP-GP2Y0A02YK0F"
date:   2017-07-23 15:00:00 -0300
categories: Arduino
---
![][1]  
Рассмотрим ещё один вариант ИК-датчика измерения расстояния **SHARP — GP2Y0A02YK0F**.  
Именно такой сенсор часто и удобно применяется в робототехнике (например, в «Poor Man's Lidar» — PML — «[лазерном дальномере бедных][2]») 

##### Принцип работы

  
Работает сенсор по тому же принципу **триангуляции**, как и похожие сенсоры SHARP (см.  
[Датчик измерения расстояния SHARP-GP2Y0A02YK0F][3])

Сенсор **SHARP-GP2Y0A02YK0F**  
![][4]

##### Размеры

  
![][5]

##### Особенности

  
**1.** Диапазон измерения расстояния: **от 20 до 150 см**  
**2.** Аналоговый выход  
**3.** Размеры: **29.5x13x21.6 мм**  
**4.** Потребление тока: **33 мА**  
**5.** Напряжение питания: **от 4.5 до 5.5 В**

![][6]

* В целях стабилизации линии питания, рекомендуется, между Vcc и GND сенсора установить конденсатор 10 uF или более.

![][7]

##### Применение

  
**1.** Безконтактный переключатель (сантехника, управление освещением и т.п.)  
**2.** Датчик экономии энергии (ATM, копиры, вендинговые машины, ноутбуки, LCD-мониторы)  
**3.** Развлекательное оборудование (роботы, аркадные игровые автоматы)

Подключается сенсор при помощи 3-пинового разъёма:  
Vo, GND, Vcc  
![][8]

Аналоговый сигнал снимается с пина **Vo**.

Сенсор имеет нелинейный выход: при линейном увеличении расстояния, сигнал на аналоговом выходе увеличивается/уменьшается нелинейно:  
![][9]  
Излом в начале графика, объясняется неспособностью дальномера обнаруживать объекты на близком расстоянии.

![][10]

##### SHARP и Arduino

Работать с этим сенсором SHARP так же просто — подключаем к нему питание и заводим вывод Vo на аналоговый вход [Arduino][11].

тестовый скетч для Arduino:  
```
    //
    // SHARP IR sensor testing
    //
    
    int IRpin = 0;                                    // аналоговый пин для подключения выхода Vo сенсора
    
    void setup() {
      Serial.begin(9600);                             // старт последовательного порта
    }
    
    void loop() {
      // 5V/1024 = 0.0048828125
      float volts = analogRead(IRpin)*0.0048828125;   // считываем значение сенсора и переводим в напряжение
      float distance = 65*pow(volts, -1.10);          // worked out from graph 65 = theretical distance / (1/Volts)S - luckylarry.co.uk
      Serial.println(distance);                       // выдаём в порт
      delay(100);                                     // ждём
    }
```
документация на **SHARP-GP2Y0A02YK0F** ([**PDF**][12])

**Ссылки:**  
[SHARP — GP2Y0A02YK0F — SENSOR, DISTANCE, ANALOGUE O/P][14]  
[Arduino – Using a Sharp IR Sensor for Distance Calculation][15]

**По теме:**  
[Программирование Arduino — аналоговый ввод/вывод][16]  
[Датчик измерения расстояния SHARP-GP2Y0A02YK0F][3]  
[Датчик измерения расстояния SHARP-GP2Y0A02YK0F — рассчёт расстояния][17]

[Source](http://robocraft.ru/blog/electronics/783.html "Permalink to Датчик измерения расстояния SHARP-GP2Y0A02YK0F / Электроника / RoboCraft. Роботы? Это просто!")

[1]: http://robocraft.ru/files/sensors/Sharp/GP2Y0A02YK0F/SHARP-GP2Y0A02YK0F-.jpg
[2]: http://robocraft.ru/blog/robosoft/742.html
[3]: http://robocraft.ru/blog/electronics/748.html
[4]: http://robocraft.ru/files/sensors/Sharp/GP2Y0A02YK0F/SHARP-GP2Y0A02YK0F.jpg
[5]: http://robocraft.ru/files/sensors/Sharp/GP2Y0A02YK0F/SHARP-GP2Y0A02YK0F-measurement.jpg
[6]: http://robocraft.ru/files/sensors/Sharp/GP2Y0A02YK0F/SHARP-GP2Y0A02YK0F-block-scheme.png
[7]: http://robocraft.ru/files/sensors/Sharp/GP2Y0A02YK0F/SHARP-GP2Y0A02YK0F-measurements.png
[8]: http://robocraft.ru/files/sensors/Sharp/GP2Y0A02YK0F/SHARP-GP2Y0A02YK0F-pinout.jpg
[9]: http://robocraft.ru/files/sensors/Sharp/GP2Y0A02YK0F/SHARP-GP2Y0A02YK0F-distance-measuring-characteristics.png
[10]: http://robocraft.ru/files/sensors/Sharp/GP2Y0A02YK0F/SHARP-GP2Y0A02YK0F-distance-measuring-characteristics2.png
[11]: http://robocraft.ru/blog/arduino/14.html
[12]: http://robocraft.ru/files/sensors/Sharp/GP2Y0A02YK0F/SHARP-GP2Y0A02YK0F.pdf
[13]: http://www.farnell.com
[14]: http://ru.farnell.com/sharp/gp20a02yk0f/sensor-distance-analogue-o-p/dp/9707891?Ntt=GP2Y0A02YK0F
[15]: http://luckylarry.co.uk/arduino-projects/arduino-using-a-sharp-ir-sensor-for-distance-calculation/
[16]: http://robocraft.ru/blog/arduino/32.html
[17]: http://robocraft.ru/blog/arduino/770.html
